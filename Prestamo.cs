﻿using System;

namespace practica_final
{
class infoprestamo
    {
        int meses,pago = 0;
        float Interesanuel, Interesmensual, sumele , cuotas,Interes,Balance,capital;
        DateTime fecha = DateTime.Today;
      
        public void DatosPrestamo()
        {
           
           
Console.WriteLine("*********************************************************");
Console.WriteLine("           SISTEMA CALCULADORA DE PRÉSTAMOS             *");
Console.WriteLine("  Nombre: Daniel Alexander Pereyra Beltran              *");
Console.WriteLine("  Matricula: 2020-9992                                  *");
Console.WriteLine("  Practica: Proyecto final.                             *");
Console.WriteLine("*********************************************************");
Console.WriteLine("----------------------------------------------------------");
Console.WriteLine("                      Bienvenido");
Console.WriteLine("----------------------------------------------------------");
Console.WriteLine("Coloque la cantidad del prestamo que quiere solicitar: ");
Balance = float.Parse(Console.ReadLine());
Console.WriteLine("Coloque la taza anual: ");
Interesanuel = float.Parse(Console.ReadLine());
Console.WriteLine("Coloque en que cantidad de meses que desea pagar:");
meses = int.Parse(Console.ReadLine());
         
        }

        public void Procesoprestamo()
        {

       Interesmensual = Interesanuel / 12;
       Interesmensual = Interesmensual / 100;
       sumele = 1 + Interesmensual;
       sumele = (float)Math.Pow(sumele , meses);
       cuotas = (sumele * Interesmensual) / (sumele - 1);
       cuotas = Balance * cuotas;
            
        }

        public void Resultadoprestamo()
        {
            Console.WriteLine("----------------------------------------------------------");
            Console.WriteLine("                     Resultado de prestamo");
            Console.WriteLine("----------------------------------------------------------");
            Console.WriteLine("Su prestamo es de:             " + Balance);
            Console.WriteLine("Con una taza anuel de un:      " + Interesanuel + "%");
            Console.WriteLine("Plazo:                         " + meses + " Meses");
            Console.WriteLine("Valor cuotas:                  RD$" + cuotas);
        }

        public void TabladeArmonizacion()
        {
            Console.WriteLine("------------------------------------------------------------------------------");

            Console.WriteLine("                            Tabla de Armonizacion");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.Write("Pago \t");
            Console.Write(" Fecha de cobro ");
            Console.Write(" Cuota \t\t");
            Console.Write("Capital \t");
            Console.Write("Interes \t");
            Console.Write("Balance \t");
            Console.WriteLine("\n");

           for (int i = 0; i < meses; i++)
         {
               pago += 1;
            Console.Write( pago + "\t");
              Console.Write(fecha.ToString("dd-MM-yy"));
              fecha = fecha.AddDays(30);
             Console.Write( "\t"  +cuotas);
             Interes = Balance * Interesmensual;
             capital = cuotas - Interes;
             Console.Write( "\t"+ capital);
             Console.Write( "\t"+ Interes);
            Balance = Balance - capital;
            Console.Write( "\t" + Balance);
            Console.WriteLine("");
            

          }
        }
        static void Main(string[] args)
        {
            infoprestamo Prestamo = new infoprestamo();
        
            Prestamo.DatosPrestamo();
            Prestamo.Procesoprestamo();
            Prestamo.Resultadoprestamo();
            Prestamo.TabladeArmonizacion();
        }
    }
}
